package computervision.grewe.opencv3_android_app;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import org.opencv.core.CvType;
import org.opencv.core.Point;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.CameraBridgeViewBase;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.android.CameraBridgeViewBase.CvCameraViewListener2;
import org.opencv.core.Mat;
import org.opencv.android.CameraBridgeViewBase.CvCameraViewFrame;
import org.opencv.imgproc.Imgproc;

import android.widget.AdapterView.OnItemSelectedListener;

import java.util.Random;


public class HelloOpenCvActivity extends AppCompatActivity implements CvCameraViewListener2, OnItemSelectedListener {
    
    
    CameraBridgeViewBase mOpenCvCameraView;// will point to our View widget for our image
    
    Spinner spinner_menu;
    
    //grab array of possible menu items from strings.xml file
    
    String[] menu_items;
    
    String menu_item_selected;
    
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hello_open_cv);
        
        //setup menu from strings.xml file
        
        this.menu_items = getResources().getStringArray(R.array.spinner_menu);
        this.menu_item_selected = menu_items[0];  //initialize to first item in arry
        
        Log.i("SPINNER", "menu item is " + this.menu_item_selected);
        
        
        //grab a handle to spinner_menu in the XML interface
        
        spinner_menu = (Spinner) findViewById(R.id.spinner_menu);
        // Create an ArrayAdapter using the string array and a default spinner layout
        
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.spinner_menu, android.R.layout.simple_spinner_item);
        // Specify the layout to use when the list of choices appears
        
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        
        spinner_menu.setAdapter(adapter);
        spinner_menu.setSelection(0);//initialize to first item in menu
        
        //set this activity to listen to the menu choice in spinner
        
        spinner_menu.setOnItemSelectedListener(this);
        
        //grab a "handle" to the OpenCV class responsible for viewing Image
        
        // look at the XML the id of our CamerBridgeViewBase is HelloOpenCVView
        
        mOpenCvCameraView = (CameraBridgeViewBase) findViewById(R.id.HelloOpenCvView);
        mOpenCvCameraView.setVisibility(CameraBridgeViewBase.VISIBLE);
        mOpenCvCameraView.setCvCameraViewListener(this);  //the activity will listen to events on Camera
        
    }
    
    
    //Code will tell us when camera connected it will enable the mOpenCVCameraView
    
    private BaseLoaderCallback mLoaderCallback = new BaseLoaderCallback(this) {
    @Override
    public void onManagerConnected(int status) {
    switch (status) {
case LoaderCallbackInterface.SUCCESS: {
    Log.i("OPENCV", "OpenCV loaded successfully");
    
    mOpenCvCameraView.enableView();
}
break;
default: {
super.onManagerConnected(status);
}
break;
}
}
};


@Override
public void onResume() {
super.onResume();
OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_3_1_0, this, mLoaderCallback);
}


@Override
public void onPause() {
super.onPause();
if (mOpenCvCameraView != null)
mOpenCvCameraView.disableView();
}


public void onDestroy() {
super.onDestroy();
if (mOpenCvCameraView != null)
mOpenCvCameraView.disableView();
}

public void onCameraViewStarted(int width, int height) {
}

public void onCameraViewStopped() {
}

// THIS IS THE main method that is called each time you get a new Frame/Image

// it should return a Mat that will be displayed in the corresponding JavaCameraView widget that should be part of the

//  xml interface for this activity that is associated with this class's variable mOpenCvCameraView  which is

//  associated with JavaCameraView widget (see onCreate method above)

public Mat onCameraFrame(CameraBridgeViewBase.CvCameraViewFrame inputFrame) {
Mat imageMat = inputFrame.rgba();


Mat gray = inputFrame.gray();


// now you use the Mat class to represent the Image and you can use method calls


// make calls like to get a pixel at i,j   imageMat.get

// double pixel[] = new double[3];

// pixel = imageMat.get(20,10);  this wil retrieve pixel and column = 20, row =10

//similarly can set a pixel in Mat  via imageMat.set(i,j,pixel);

// read API on Mat class for OPenCV


// A VERY USEFUL class to call image processing routines is ImagProc

// This code in comments shows how to do the Sobel Edge Detection on our image in imageMat

/*
 
 Mat gray = inputFrame.gray();
 
 Mat mIntermediateMat = new Mat();
 
 Imgproc.Sobel(gray, mIntermediateMat, CvType.CV_8U, 1, 1);
 
 Core.convertScaleAbs(mIntermediateMat, mIntermediateMat, 10, 0);
 
 Imgproc.cvtColor(mIntermediateMat, imageMat, Imgproc.COLOR_GRAY2BGRA, 4);
 
 
 
 */
if(this.menu_item_selected.equals("Random")) {   //Random

//return imageMat;
//create random number 0 to 1 and return color if < .5 and grey otherwise
Random rand = new Random(System.currentTimeMillis());

if (rand.nextDouble() < 0.5)
{    Log.d("SPINNER", "return color");  return imageMat;}
else
{    Log.d("SPINNER", "return greyscale"); return gray;}
}
else if(this.menu_item_selected.equals("Greyscale")) { //Greyscale
Log.d("SPINNER", "return greyscale");
return gray;
}
else if (this.menu_item_selected.equals("Edgy"))
{
int pixel=0;
int threshold=230;
double lap[]=new double[]{

-1.0, -1.0 , -1.0 ,
-1.0, 8.0 , -1.0 ,
-1.0, -1.0 , -1.0


};
byte[]c00= new byte[4];
byte[] c01= new byte[4];
byte[] c02= new byte[4];
byte[] c10= new byte[4];
byte[] c11= new byte[4];
byte[] c12= new byte[4];
byte[] c20= new byte[4];
byte[] c21= new byte[4];
byte[] c22= new byte[4];

for(int y =1; y< gray.cols() - 1;y++){
for(int x=1; x< gray.rows() - 1; x++)
{
gray.get(x-1 , y-1, c00);
gray.get(x-1 , y, c01);
gray.get(x-1 , y+1, c02);
gray.get(x , y-1, c10);
gray.get(x , y, c11);
gray.get(x , y+1, c12);
gray.get(x+1 ,y-1, c20);
gray.get(x+1 ,y, c21);
gray.get(x+1 , y+1, c22);
double r= lap[0]* c00[0]+lap[1]* c01[0]+lap[2]*c02[0]+
lap[3]* c10[0]+lap[4]* c11[0]+lap[5]* c12[0]+
lap[6]* c20[0]+lap[7]* c21[0]+lap[8]*c22[0];
double g= lap[0]*  c00[1]+lap[1]* c01[1]+lap[2]* c02[1]+
lap[3]* c10[1]+lap[4]* c11[1]+lap[5]* c12[1]+
lap[6]* c20[1]+lap[7]* c21[1]+lap[8]*c22[1];
double b= lap[0]* c00[2]+lap[1]* c01[2]+lap[2]* c02[2]+
lap[3]* c10[2]+lap[4]* c11[2]+lap[5]* c12[2]+
lap[6]* c20[2]+lap[7]* c21[2]+lap[8]* c22[2];
double a= lap[0]* c00[3]+lap[1]* c01[3]+lap[2]* c02[3]+
lap[3]* c10[3]+lap[4]* c11[3]+lap[5]* c12[3]+
lap[6]* c20[3]+lap[7]* c21[3]+lap[8]* c22[3];
r=Math.min(255, Math.max(0, r));
g=Math.min(255, Math.max(0, g));
b=Math.min(255, Math.max(0, b));
a=Math.min(255, Math.max(0, a));

if(r>threshold)
{
pixel=255;

}
else
pixel=0;
if(pixel==255)
{
c00[0] = (byte) (c00[0] & 255);
c00[1] = (byte) (c00[1] & 0);
c00[2] = (byte) (c00[2] & 0);
c00[3] = (byte) (c00[3] & 1);
imageMat.put(x,y, 255, 0, 0, 1);

}
else{
imageMat.put(x,y, c00[0], c00[1], c00[2], c00[3]);
}


}
}




return imageMat;


}
else if (this.menu_selected_item.equals("PeopleDetection") {
Mat grayMat = new Mat();
Mat people = new Mat();

Imgproc.cvtColor(imageMat, grayMat,
Imgproc.COLOR_BGR2GRAY);
HOGDescriptor hog = new HOGDescriptor();
hog.setSVMDetector(HOGDescriptor
.getDefaultPeopleDetector());
MatOfRect faces = new MatOfRect();
MatOfDouble weights = new MatOfDouble();
hog.detectMultiScale(grayMat, faces, weights);
originalMat.copyTo(people);
//Draw faces on the image
Rect[] facesArray = faces.toArray();
for (int i = 0; i < facesArray.length; i++)
Core.rectangle(people, facesArray[i].tl(),
facesArray[i].br(), new Scalar(100), 3);
return people;

}


else if (this.menu_item_selected.equals("Best Lines")) {

int count=0;
int pixel=0;
int threshold=230;
double lap[]=new double[]{

-1.0, -1.0 , -1.0 ,
-1.0, 8.0 , -1.0 ,
-1.0, -1.0 , -1.0


};
byte[]c00= new byte[4];
byte[] c01= new byte[4];
byte[] c02= new byte[4];
byte[] c10= new byte[4];
byte[] c11= new byte[4];
byte[] c12= new byte[4];
byte[] c20= new byte[4];
byte[] c21= new byte[4];
byte[] c22= new byte[4];

Mat in = new Mat(gray.height() , gray.cols() , CvType.CV_8UC1);

for(int y =1; y< gray.cols() - 1;y++){
for(int x=1; x< gray.rows() - 1; x++)
{
gray.get(x-1 , y-1, c00);
gray.get(x-1 , y, c01);
gray.get(x-1 , y+1, c02);
gray.get(x , y-1, c10);
gray.get(x , y, c11);
gray.get(x , y+1, c12);
gray.get(x+1 ,y-1, c20);
gray.get(x+1 ,y, c21);
gray.get(x+1 , y+1, c22);
double r= lap[0]* c00[0]+lap[1]* c01[0]+lap[2]*c02[0]+
lap[3]* c10[0]+lap[4]* c11[0]+lap[5]* c12[0]+
lap[6]* c20[0]+lap[7]* c21[0]+lap[8]*c22[0];
double g= lap[0]*  c00[1]+lap[1]* c01[1]+lap[2]* c02[1]+
lap[3]* c10[1]+lap[4]* c11[1]+lap[5]* c12[1]+
lap[6]* c20[1]+lap[7]* c21[1]+lap[8]*c22[1];
double b= lap[0]* c00[2]+lap[1]* c01[2]+lap[2]* c02[2]+
lap[3]* c10[2]+lap[4]* c11[2]+lap[5]* c12[2]+
lap[6]* c20[2]+lap[7]* c21[2]+lap[8]* c22[2];
double a= lap[0]* c00[3]+lap[1]* c01[3]+lap[2]* c02[3]+
lap[3]* c10[3]+lap[4]* c11[3]+lap[5]* c12[3]+
lap[6]* c20[3]+lap[7]* c21[3]+lap[8]* c22[3];
r=Math.min(255, Math.max(0, r));
g=Math.min(255, Math.max(0, g));
b=Math.min(255, Math.max(0, b));
a=Math.min(255, Math.max(0, a));
in.put(x, y, r, g, b, a);
}
}

Imgproc.threshold(in, in, 50.0, 255.0, Imgproc.THRESH_BINARY);

Mat lines = new Mat();

int thres_line = Math.min(gray.cols(), gray.rows()) / 6;
Imgproc.HoughLines(in, lines, 1, Math.PI / 180, thres_line);
for (int i = 0; i < lines.rows(); i++) {

double[] values = lines.get(i, 0);
double rho = values[0];
double theta = values[1];
double a = Math.cos(theta);
double b = Math.sin(theta);
double x0 = a * rho;
double y0 = b * rho;
double x1, y1, x2, y2;
x1 = Math.round(x0 + 10000 * (-b));
y1 = Math.round(y0 + 10000 * a);
x2 = Math.round(x0 - 10000 * (-b));
y2 = Math.round(y0 - 10000 * a);
Point pt1 = new Point(x1, y1);
Point pt2 = new Point(x2, y2);
Imgproc.line(imageMat, pt1, pt2, new Scalar(0, 0, 255), 1);
if (((y2-y1)/(x2-x1) )<=0.57335) {

if (count <= 10) {

Imgproc.line(imageMat, pt1, pt2, new Scalar(255, 0, 0, 1), 2);
count++;
}
}
}




return imageMat;
}

else  //for now return color for all other choices

{
return gray;
}

}
//Spinner Menu Selection response method

public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
// An item was selected. You can retrieve the selected item using

this.menu_item_selected = parent.getItemAtPosition(pos).toString();

Log.i("SPINNER", "choice is" + this.menu_item_selected);


}


public void onNothingSelected(AdapterView<?> parent) {
this.menu_item_selected = this.menu_items[0];


}
}
